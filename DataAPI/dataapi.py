#from flask import Flask
import pandas as pd
import pickle
import lzma
import numpy as np
import json
import re
import requests
#from flask import Flask, jsonify
#from flask import request
from flask_cors import CORS, cross_origin
import urllib.request
#import requests
import ssl
from pandas.io.json import json_normalize
#pd.options.mode.chained_assignment = None
import os
from flask import Flask, abort, request, jsonify, g, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
import math

app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

Fin_data = pd.read_csv('Fin_data_test.csv', sep='\t')
scrap_data = pd.read_csv('scrap_data_test.csv', sep='\t')
spc_scrap_data = pd.read_csv('spc_scrap_data_test.csv', sep='\t')

@app.route('/')
def index():
    return "Welcome to the Popertee Data API! Contact tariq@popertee.com for more details!"



@app.route('/pro/discoverdata', methods=['POST'])
def pro_get_task():
    data = json.loads(request.data, strict=False)


    gen = data['query']['gender']
    age = data['query']['age']
    spend = data['query']['spend']
    visit = data['query']['visitor']
    url = data['poperteeApiLink']
    acctok = data['accessToken']
    useag = data['userAgent']

    if acctok is None:
        acctok = ''
        useag = ''

    ssl._create_default_https_context = ssl._create_unverified_context
    hdr = { 'User-Agent':useag,'Popertee-Token' :acctok}
    req = urllib.request.Request(url, headers=hdr)
    response = urllib.request.urlopen(req)

    space_data = json.load(response)
    space_call = space_data['spaces']
    space_data_norm = json_normalize(space_data['spaces'])

    if len(space_call) == 0:
        filter_space=['ntohignhere7464']
        space_call = []
    else:
        filter_space = space_data_norm['_id'].values.tolist()
    
    fil_space_q = scrap_data.space_name.isin(filter_space)
    Fin_data2 = scrap_data[fil_space_q]

    if len(Fin_data2) > 0:
        
        if len(gen) > 0:
            gen_q = Fin_data2.gender.isin(gen)
        else:
            gen_q = Fin_data2.gender.isin([0,1])

        if len(age) > 0:
            age_q = Fin_data2.age.isin(age)
        else:
            age_q = Fin_data2.age.isin([0,1,2,3,4,5])
    
        if len(spend) > 0:
            spend_q = Fin_data2.affluence.isin(spend)
        else:
            spend_q = Fin_data2.affluence.isin([0,1,2,3,4])

        if len(visit) > 0:
            visit_q = Fin_data2.visitor_type.isin(visit)
        else:
            visit_q = Fin_data2.visitor_type.isin([0,1,2])

        targ_foot = Fin_data2[gen_q & age_q & spend_q & visit_q]
        Fin_data2['targ_ind'] = np.where(gen_q & age_q & spend_q & visit_q==True,1,0)
        targ_foot = targ_foot.groupby(['space_name']).sum().reset_index()
        targ_foot['targ_volume']=((targ_foot['volume']-min(targ_foot['volume']))/(max(targ_foot['volume'])-min(targ_foot['volume'])))
    
        targ_foot = targ_foot[['space_name','targ_volume','volume']]
    
        tot_foot = Fin_data2[['space_name','volume','postsect','ps_footfall']]
        tot_foot = tot_foot.groupby(['space_name','postsect','ps_footfall']).sum().reset_index()
        tot_foot['tot_volume']=((tot_foot['volume']-min(tot_foot['volume']))/(max(tot_foot['volume'])-min(tot_foot['volume'])))
        tot_foot = tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']]

        #####Combine total and target and create percentage of target
        Rank_data = pd.merge(targ_foot,
                        tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']],
                            left_on=['space_name'],
                            right_on=['space_name'],
                            how='left')
    
        Rank_data['perc_targ']=((Rank_data['volume_x'])/(Rank_data['volume_y']))
        Rank_data['volume_x']=round((Rank_data['volume_x'])/7)
        Rank_data['volume_y']=round((Rank_data['volume_y'])/7)
        Rank_data['perc_targ_st']=((Rank_data['perc_targ']-min(Rank_data['perc_targ']))/(max(Rank_data['perc_targ'])-min(Rank_data['perc_targ'])))
    
        #########Ranking Algorithm
        Rank_data['score']=(Rank_data['targ_volume']*.5)+(Rank_data['tot_volume']*.1)+(Rank_data['perc_targ_st']*.4)

        Rank_data = Rank_data.sort_values(by=['score', 'volume_y'], ascending=False).reset_index()

        Rank_data['Rank'] = Rank_data.index +1

        Rank_data2 = Rank_data[['space_name','Rank','volume_x','volume_y','perc_targ','score','postsect','ps_footfall']]
        

        fin_gen = scrap_data[['space_name','gender','volume']]
        fin_gen_sum = fin_gen.groupby(['space_name','gender']).sum().reset_index()
        fin_gen_m = fin_gen_sum[fin_gen_sum['gender']==0].reset_index()
        fin_gen_f = fin_gen_sum[fin_gen_sum['gender']==1].reset_index()

        fin_gen_m = fin_gen_m[['space_name','volume']]
        fin_gen_f = fin_gen_f[['space_name','volume']] 

        gen_fin = pd.merge(fin_gen_m,
                fin_gen_f[['space_name','volume']],
                       left_on=['space_name'],
                       right_on=['space_name'],
                       how='left')
        gen_fin['Male'] = round((gen_fin['volume_x']/(gen_fin['volume_x']+gen_fin['volume_y'])),2)
        gen_fin['Female']= 1-gen_fin['Male']
        gen_fin = gen_fin[['space_name','Male','Female']]

        new_Rank_data = pd.merge(Rank_data2,
                gen_fin[['space_name','Male','Female']],
                       left_on=['space_name'],
                       right_on=['space_name'],
                       how='left')

#percentage for each age group
        age_top = Fin_data2[['space_name','age','volume']]
        age_top_sum = age_top.groupby(['space_name','age']).sum().reset_index()
        per_cre = age_top_sum.groupby(['space_name','age']).agg({'volume': 'sum'})
        age_top_per = per_cre.groupby(level=0).apply(lambda x:
                                                 100 * x / float(x.sum())).reset_index()
        age_top_per['percent'] = round(age_top_per['volume'])
        if len(age) > 0:
            age_qt = age_top_per.age.isin(age)
        else:
            age_qt = age_top_per.age.isin([0,1,2,3,4,5])
    
        age_top_per = age_top_per[age_qt]
        age_top_per = age_top_per.loc[age_top_per.groupby('space_name').volume.idxmax()].reset_index()
        
        data_output = pd.merge(new_Rank_data,
                age_top_per[['space_name','age','percent']],
                        left_on=['space_name'],
                        right_on=['space_name'],
                        how='left')

        new_Rank_data_30 = data_output.head(120)
        Rank_data_merge = Rank_data[['space_name','Rank']]
#    Rank_data = Rank_data.head(1)
    ######### Call Data Creation
        call_datac = pd.merge(Rank_data_merge,
                        Fin_data2[['space_name','postsect','Weekday_label','day_part','visitor_type','gender','age','affluence','volume']],
                            left_on=['space_name'],
                            right_on=['space_name'],
                            how='left')

        jsonfiles = json.loads(new_Rank_data_30.to_json(orient='records'))
        data_return={}
        data_return['data']=jsonfiles
        data_return['spaces']=space_call
    else:
        data_return={}
        data_return['spaces']=space_call
    return jsonify(data_return)



@app.route('/pro/spacedata', methods=['POST'])
#@auth.login_required
def pro_spacetask():

    data = json.loads(request.data, strict=False)

    gen = data['query']['gender']
    age = data['query']['age']
    spend = data['query']['spend']
    visit = data['query']['visitor']
    space = data['space']['id']
    url = data['poperteeApiLink']
    acctok = data['accessToken']
    useag = data['userAgent']

    if acctok is None:
        acctok = ''
        useag = ''

    ssl._create_default_https_context = ssl._create_unverified_context

    hdr = { 'User-Agent':useag,'Popertee-Token' :acctok}
    req = urllib.request.Request(url, headers=hdr)
    response = urllib.request.urlopen(req)

    space_datapre = json.load(response)
    space_data = space_datapre['space']
    space_data_norm = json_normalize(space_datapre	['space'])

    filter_space = space_data_norm['_id'].values.tolist()
    
    fil_space_q = scrap_data.space_name.isin(filter_space)
    Fin_data2 = scrap_data[fil_space_q]


    if len(gen) > 0:
       gen_q = Fin_data2.gender.isin(gen)
    else:
        gen_q = Fin_data2.gender.isin([0,1])

    if len(age) > 0:
        age_q = Fin_data2.age.isin(age)
    else:
        age_q = Fin_data2.age.isin([0,1,2,3,4,5])

    if len(spend) > 0:
        spend_q = Fin_data2.affluence.isin(spend)
    else:
        spend_q = Fin_data2.affluence.isin([0,1,2,3,4])

    if len(visit) > 0:
        visit_q = Fin_data2.visitor_type.isin(visit)
    else:
        visit_q = Fin_data2.visitor_type.isin([0,1,2])


    targ_foot = Fin_data2[gen_q & age_q & spend_q & visit_q]
    Fin_data2['targ_ind'] = np.where(gen_q & age_q & spend_q & visit_q==True,1,0)
    targ_foot = targ_foot.groupby(['space_name']).sum().reset_index()
    targ_foot['targ_volume']=((targ_foot['volume']-min(targ_foot['volume']))/(max(targ_foot['volume'])-min(targ_foot['volume'])))

    targ_foot = targ_foot[['space_name','targ_volume','volume']]

    tot_foot = Fin_data2[['space_name','volume','postsect','ps_footfall']]
    tot_foot = tot_foot.groupby(['space_name','postsect','ps_footfall']).sum().reset_index()
    tot_foot['tot_volume']=((tot_foot['volume']-min(tot_foot['volume']))/(max(tot_foot['volume'])-min(tot_foot['volume'])))
    tot_foot = tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']]

    #####Combine total and target and create percentage of target
    Rank_data = pd.merge(targ_foot,
                    tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']],
                        left_on=['space_name'],
                        right_on=['space_name'],
                        how='left')

    Rank_data['perc_targ']=((Rank_data['volume_x'])/(Rank_data['volume_y']))
    Rank_data['perc_targ_st']=((Rank_data['perc_targ']-min(Rank_data['perc_targ']))/(max(Rank_data['perc_targ'])-min(Rank_data['perc_targ'])))

    #########Ranking Algorithm
    Rank_data['score']=(Rank_data['targ_volume']*.5)+(Rank_data['tot_volume']*.2)+(Rank_data['perc_targ_st']*.3)
    Rank_data = Rank_data.sort_values(by=['score'], ascending=False).reset_index()
    Rank_data['Rank'] = Rank_data.index +1

    Rank_data = Rank_data[['space_name','Rank','volume_x','volume_y','perc_targ','score','postsect','ps_footfall']]



    Rank_data_merge = Rank_data[['space_name','Rank']]
#    Rank_data = Rank_data.head(1)
    ######### Call Data Creation
    call_datac = pd.merge(Rank_data_merge,
                    Fin_data2[['space_name','postsect','Weekday_label','day_part','visitor_type','gender','age','affluence','volume','targ_ind']],
                        left_on=['space_name'],
                        right_on=['space_name'],
                        how='left')

    space_q = call_datac.space_name.isin(space)
    space_info = call_datac[space_q]

    space_info_f=space_info.copy()
    space_info['volume']=space_info['volume']/7
    af_tot = space_info[['space_name','affluence','volume']]
    af_targ = space_info[['space_name','affluence','volume','targ_ind']]
    ff_tot = space_info_f[['space_name','Weekday_label','volume']]
    ff_targ = space_info_f[['space_name','Weekday_label','volume','targ_ind']]
    gen_tot = space_info[['space_name','gender','volume']] 
    gen_targ = space_info[['space_name','gender','volume','targ_ind']]
    age_tot = space_info[['space_name','age','volume']]
    age_targ = space_info[['space_name','age','volume','targ_ind']]

    Af_tot = af_tot.groupby(['space_name','affluence']).sum().reset_index() 
    Af_targ = af_targ.groupby(['space_name','affluence','targ_ind']).sum().reset_index()
    Af_targ = Af_targ[Af_targ.targ_ind == 1]
    Af_targ = Af_targ[['space_name','affluence','volume']]
    Gen_tot = gen_tot.groupby(['space_name','gender']).sum().reset_index()
    Gen_targ = gen_targ.groupby(['space_name','gender','targ_ind']).sum().reset_index()
    Gen_targ = Gen_targ[Gen_targ.targ_ind == 1]
    Gen_targ = Gen_targ[['space_name','gender','volume']]
    Age_tot = age_tot.groupby(['space_name','age']).sum().reset_index()
    Age_targ = age_targ.groupby(['space_name','age','targ_ind']).sum().reset_index()
    Age_targ = Age_targ[Age_targ.targ_ind == 1]
    Age_targ = Age_targ[['space_name','age','volume']]
    Ff_sec_tot = ff_tot.groupby(['space_name','Weekday_label']).sum().reset_index()
    Ff_sec_targ = ff_targ.groupby(['space_name','Weekday_label','targ_ind']).sum().reset_index()
    Ff_sec_targ = Ff_sec_targ[Ff_sec_targ.targ_ind == 1]
    Ff_sec_targ = Ff_sec_targ[['space_name','Weekday_label','volume']]

    Af_sec = pd.merge(Af_targ,
                    Af_tot[['space_name','affluence','volume']],
                           left_on=['space_name','affluence'],
                           right_on=['space_name','affluence'],
                           how='right')
    Af_sec = Af_sec.fillna(0)
    if len(spend) > 0:
        Af_p = Af_sec['affluence'].isin(spend)
    else:
        Af_p = Af_sec['affluence'].isin([0,1,2,3,4])
    Af_sec['targ_group'] = np.where(Af_p==True, Af_sec['volume_y'], 0)
    Af_per1 = Af_sec[['space_name','targ_group','volume_y']]
    Af_per = Af_per1.groupby(['space_name']).sum().reset_index()
    Af_per['targ_perc'] = round(((Af_per['targ_group']/Af_per['volume_y'])),2)

    Gen_sec = pd.merge(Gen_targ,
                    Gen_tot[['space_name','gender','volume']],
                           left_on=['space_name','gender'],
                           right_on=['space_name','gender'],
                           how='right')
    Gen_sec = Gen_sec.fillna(0)

    Age_sec = pd.merge(Age_targ,
                    Age_tot[['space_name','age','volume']],
                           left_on=['space_name','age'],
                           right_on=['space_name','age'],
                           how='right')
    Age_sec = Age_sec.fillna(0)

    if len(age) > 0:
        age_p = Age_sec['age'].isin(age)
    else:
        age_p = Age_sec['age'].isin([0,1,2,3,4,5])
    Age_sec['targ_group'] = np.where(age_p==True, Age_sec['volume_y'], 0)
    age_per1 = Age_sec[['space_name','targ_group','volume_y']]
    age_per = age_per1.groupby(['space_name']).sum().reset_index()
    age_per['targ_perc'] = round(((age_per['targ_group']/age_per['volume_y'])),2)


    Ff_sec = pd.merge(Ff_sec_targ,
                    Ff_sec_tot[['space_name','Weekday_label','volume']],
                           left_on=['space_name','Weekday_label'],
                           right_on=['space_name','Weekday_label'],
                           how='right')
    Ff_sec = Ff_sec.fillna(0)


    gen_per = space_info[['space_name','gender','volume']]
    fin_gen_sum = gen_per.groupby(['space_name','gender']).sum().reset_index()
    fin_gen_m = fin_gen_sum[fin_gen_sum['gender']==0].reset_index()
    fin_gen_f = fin_gen_sum[fin_gen_sum['gender']==1].reset_index()
    fin_gen_m = fin_gen_m[['space_name','volume']]
    fin_gen_f = fin_gen_f[['space_name','volume']]
    gen_fin = pd.merge(fin_gen_m,
                     fin_gen_f[['space_name','volume']],
                           left_on=['space_name'],
                           right_on=['space_name'],
                           how='left')

    gen_fin['Male'] = round((gen_fin['volume_x']/(gen_fin['volume_x']+gen_fin['volume_y'])),2)
    gen_fin['Female']= 1-gen_fin['Male']
    gen_fin = gen_fin[['space_name','Male','Female']]

#top age groups
    age_top = Fin_data2[['space_name','age','volume']]
    age_top_sum = age_top.groupby(['space_name','age']).sum().reset_index()
    per_cre = age_top_sum.groupby(['space_name','age']).agg({'volume': 'sum'})
    age_top_per = per_cre.groupby(level=0).apply(lambda x:
                                            100 * x / float(x.sum())).reset_index()
    age_top_per['percent'] = round(age_top_per['volume'])
    if len(age) > 0:
        age_qt = age_top_per.age.isin(age)
    else:
        age_qt = age_top_per.age.isin([0,1,2,3,4,5])    
    age_top_per = age_top_per[age_qt]
    age_top_per = age_top_per.loc[age_top_per.groupby('space_name').volume.idxmax()].reset_index()


    age_top = json.loads(age_top_per.to_json(orient='records')) 
    ff = json.loads(Ff_sec.to_json(orient='records'))
    Age = json.loads(Age_sec.to_json(orient='records'))
    Ageper = json.loads(age_per.to_json(orient='records'))
    Gen = json.loads(Gen_sec.to_json(orient='records'))
    Af = json.loads(Af_sec.to_json(orient='records'))
    Afper = json.loads(Af_per.to_json(orient='records'))
    gen_per = json.loads(gen_fin.to_json(orient='records')) 
    data_return={}
    data_return['Footfall']=ff 
    data_return['Age']=Age
    data_return['Gender']=Gen
    data_return['Affluence']=Af
    data_return['Gen_Per']=gen_per
    data_return['Age_per']=Ageper
    data_return['Age_top']=age_top
    data_return['Spend_Per']=Afper


    return jsonify(data_return)


@app.route('/spc/discoverdata', methods=['POST'])
#@auth.login_required
def spc_get_task():

    data = json.loads(request.data, strict=False)


    gen = data['query']['gender']
    age = data['query']['age']
    spend = data['query']['spend']
    visit = data['query']['visitor']
#    week = data['query']['week_time']
    url = data['poperteeApiLink']
    acctok = data['accessToken']
    useag = data['userAgent']



    ssl._create_default_https_context = ssl._create_unverified_context
  #  contents = urllib.request.urlopen(url)
    
    hdr = { 'User-Agent':useag,'Authorization' :acctok}
    req = urllib.request.Request(url, headers=hdr)
    response = urllib.request.urlopen(req)
 #   scrap_call = json.loads(response)

    space_data = json.load(response)
    space_call = space_data['spaces']
    space_data_norm = json_normalize(space_data['spaces'])

    if len(space_call) == 0:
        filter_space=['ntohignhere7464']
        space_call = []
    else:
        filter_space = space_data_norm['_id'].values.tolist()
    
    fil_space_q = spc_scrap_data.space_name.isin(filter_space)
    Fin_data2 = spc_scrap_data[fil_space_q]

    if len(Fin_data2) > 0:
        
        if len(gen) > 0:
            gen_q = Fin_data2.gender.isin(gen)
        else:
            gen_q = Fin_data2.gender.isin([0,1])

        if len(age) > 0:
            age_q = Fin_data2.age.isin(age)
        else:
            age_q = Fin_data2.age.isin([0,1,2,3,4,5])
    
        if len(spend) > 0:
            spend_q = Fin_data2.affluence.isin(spend)
        else:
            spend_q = Fin_data2.affluence.isin([0,1,2,3,4])

        if len(visit) > 0:
            visit_q = Fin_data2.visitor_type.isin(visit)
        else:
            visit_q = Fin_data2.visitor_type.isin([0,1,2])

    
        targ_foot = Fin_data2[gen_q & age_q & spend_q & visit_q]

        Fin_data2['targ_ind'] = np.where(gen_q & age_q & spend_q & visit_q==True,1,0)

        targ_foot = targ_foot.groupby(['space_name']).sum().reset_index()
        targ_foot['targ_volume']=((targ_foot['volume']-min(targ_foot['volume']))/(max(targ_foot['volume'])-min(targ_foot['volume'])))
    
        targ_foot = targ_foot[['space_name','targ_volume','volume']]
    
        tot_foot = Fin_data2[['space_name','volume','postsect','ps_footfall']]
        tot_foot = tot_foot.groupby(['space_name','postsect','ps_footfall']).sum().reset_index()
        tot_foot['tot_volume']=((tot_foot['volume']-min(tot_foot['volume']))/(max(tot_foot['volume'])-min(tot_foot['volume'])))
        tot_foot = tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']]

        #####Combine total and target and create percentage of target
        Rank_data = pd.merge(targ_foot,
                        tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']],
                            left_on=['space_name'],
                            right_on=['space_name'],
                            how='left')
    
        Rank_data['perc_targ']=((Rank_data['volume_x'])/(Rank_data['volume_y']))
        Rank_data['volume_x']=round((Rank_data['volume_x'])/7)
        Rank_data['volume_y']=round((Rank_data['volume_y'])/7)
        Rank_data['perc_targ_st']=((Rank_data['perc_targ']-min(Rank_data['perc_targ']))/(max(Rank_data['perc_targ'])-min(Rank_data['perc_targ'])))
        Rank_data["perc_targ_st"].fillna(0, inplace = True)
 
        #########Ranking Algorithm
        Rank_data['score']=(Rank_data['targ_volume']*.5)+(Rank_data['tot_volume']*.1)+(Rank_data['perc_targ_st']*.4)

        Rank_data = Rank_data.sort_values(by=['score', 'volume_y'], ascending=False).reset_index()

        Rank_data['Rank'] = Rank_data.index +1

        Rank_data2 = Rank_data[['space_name','Rank','volume_x','volume_y','perc_targ','score','postsect','ps_footfall']]
        

        fin_gen = spc_scrap_data[['space_name','gender','volume']]
        fin_gen_sum = fin_gen.groupby(['space_name','gender']).sum().reset_index()
        fin_gen_m = fin_gen_sum[fin_gen_sum['gender']==0].reset_index()
        fin_gen_f = fin_gen_sum[fin_gen_sum['gender']==1].reset_index()

        fin_gen_m = fin_gen_m[['space_name','volume']]
        fin_gen_f = fin_gen_f[['space_name','volume']] 


        gen_fin = pd.merge(fin_gen_m,
                fin_gen_f[['space_name','volume']],
                       left_on=['space_name'],
                       right_on=['space_name'],
                       how='left')
        gen_fin['Male'] = round((gen_fin['volume_x']/(gen_fin['volume_x']+gen_fin['volume_y'])),2)
        gen_fin['Female']= 1-gen_fin['Male']
        gen_fin = gen_fin[['space_name','Male','Female']]

        new_Rank_data = pd.merge(Rank_data2,
                gen_fin[['space_name','Male','Female']],
                       left_on=['space_name'],
                       right_on=['space_name'],
                       how='left')

#percentage for each age group
        age_top = Fin_data2[['space_name','age','volume']]
        age_top_sum = age_top.groupby(['space_name','age']).sum().reset_index()
        per_cre = age_top_sum.groupby(['space_name','age']).agg({'volume': 'sum'})
        age_top_per = per_cre.groupby(level=0).apply(lambda x:
                                                 100 * x / float(x.sum())).reset_index()
        age_top_per['percent'] = round(age_top_per['volume'])
        if len(age) > 0:
            age_qt = age_top_per.age.isin(age)
        else:
            age_qt = age_top_per.age.isin([0,1,2,3,4,5])
    
        age_top_per = age_top_per[age_qt]
        age_top_per = age_top_per.loc[age_top_per.groupby('space_name').volume.idxmax()].reset_index()


        data_output = pd.merge(new_Rank_data,
                age_top_per[['space_name','age','percent']],
                        left_on=['space_name'],
                        right_on=['space_name'],
                        how='left')


        new_Rank_data_30 = data_output.head(301)
        Rank_data_merge = Rank_data[['space_name','Rank']]

    ######### Call Data Creation
        call_datac = pd.merge(Rank_data_merge,
                        Fin_data2[['space_name','postsect','Weekday_label','day_part','visitor_type','gender','age','affluence','volume']],
                            left_on=['space_name'],
                            right_on=['space_name'],
                            how='left')

        jsonfiles = json.loads(new_Rank_data_30.to_json(orient='records'))
        data_return={}
        data_return['data']=jsonfiles
        data_return['spaces']=space_call
    else:
        data_return={}
        data_return['spaces']=space_call
    return jsonify(data_return)



@app.route('/spc/spacedata', methods=['POST'])
#@auth.login_required
def spc_spacetask():

    data = json.loads(request.data, strict=False)

    gen = data['query']['gender']
    age = data['query']['age']
    spend = data['query']['spend']
    visit = data['query']['visitor']
    space = data['space']['id']
    url = data['poperteeApiLink']
    acctok = data['accessToken']
    useag = data['userAgent']



    ssl._create_default_https_context = ssl._create_unverified_context

    hdr = { 'User-Agent':useag,'Authorization' :acctok}
    req = urllib.request.Request(url, headers=hdr)
    response = urllib.request.urlopen(req)


    space_datapre = json.load(response)
    space_data = space_datapre['space']
    space_data_norm = json_normalize(space_datapre['space'])

    filter_space = space_data_norm['_id'].values.tolist()
    
    fil_space_q = spc_scrap_data.space_name.isin(filter_space)
    Fin_data2 = spc_scrap_data[fil_space_q]


    if len(gen) > 0:
       gen_q = Fin_data2.gender.isin(gen)
    else:
        gen_q = Fin_data2.gender.isin([0,1])

    if len(age) > 0:
        age_q = Fin_data2.age.isin(age)
    else:
        age_q = Fin_data2.age.isin([0,1,2,3,4,5])

    if len(spend) > 0:
        spend_q = Fin_data2.affluence.isin(spend)
    else:
        spend_q = Fin_data2.affluence.isin([0,1,2,3,4])

    if len(visit) > 0:
        visit_q = Fin_data2.visitor_type.isin(visit)
    else:
        visit_q = Fin_data2.visitor_type.isin([0,1,2])


    targ_foot = Fin_data2[gen_q & age_q & spend_q & visit_q]
    Fin_data2['targ_ind'] = np.where(gen_q & age_q & spend_q & visit_q==True,1,0)
    targ_foot = targ_foot.groupby(['space_name']).sum().reset_index()
    targ_foot['targ_volume']=((targ_foot['volume']-min(targ_foot['volume']))/(max(targ_foot['volume'])-min(targ_foot['volume'])))

    targ_foot = targ_foot[['space_name','targ_volume','volume']]

    tot_foot = Fin_data2[['space_name','volume','postsect','ps_footfall']]
    tot_foot = tot_foot.groupby(['space_name','postsect','ps_footfall']).sum().reset_index()
    tot_foot['tot_volume']=((tot_foot['volume']-min(tot_foot['volume']))/(max(tot_foot['volume'])-min(tot_foot['volume'])))
    tot_foot = tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']]

    #####Combine total and target and create percentage of target
    Rank_data = pd.merge(targ_foot,
                    tot_foot[['space_name','tot_volume','volume','postsect','ps_footfall']],
                        left_on=['space_name'],
                        right_on=['space_name'],
                        how='left')

    Rank_data['perc_targ']=((Rank_data['volume_x'])/(Rank_data['volume_y']))
    Rank_data['perc_targ_st']=((Rank_data['perc_targ']-min(Rank_data['perc_targ']))/(max(Rank_data['perc_targ'])-min(Rank_data['perc_targ'])))

    #########Ranking Algorithm
    Rank_data['score']=(Rank_data['targ_volume']*.5)+(Rank_data['tot_volume']*.2)+(Rank_data['perc_targ_st']*.3)
    Rank_data = Rank_data.sort_values(by=['score'], ascending=False).reset_index()
    Rank_data['Rank'] = Rank_data.index +1

    Rank_data = Rank_data[['space_name','Rank','volume_x','volume_y','perc_targ','score','postsect','ps_footfall']]



    Rank_data_merge = Rank_data[['space_name','Rank']]
#    Rank_data = Rank_data.head(1)
    ######### Call Data Creation
    call_datac = pd.merge(Rank_data_merge,
                    Fin_data2[['space_name','postsect','Weekday_label','day_part','visitor_type','gender','age','affluence','volume','targ_ind']],
                        left_on=['space_name'],
                        right_on=['space_name'],
                        how='left')

    space_q = call_datac.space_name.isin(space)
    space_info = call_datac[space_q]

    space_info_f=space_info.copy()
    space_info['volume']=space_info['volume']/7
    af_tot = space_info[['space_name','affluence','volume']]
    af_targ = space_info[['space_name','affluence','volume','targ_ind']]
    ff_tot = space_info_f[['space_name','Weekday_label','volume']]
    ff_targ = space_info_f[['space_name','Weekday_label','volume','targ_ind']]
    gen_tot = space_info[['space_name','gender','volume']] 
    gen_targ = space_info[['space_name','gender','volume','targ_ind']]
    age_tot = space_info[['space_name','age','volume']]
    age_targ = space_info[['space_name','age','volume','targ_ind']]

    Af_tot = af_tot.groupby(['space_name','affluence']).sum().reset_index() 
    Af_targ = af_targ.groupby(['space_name','affluence','targ_ind']).sum().reset_index()
    Af_targ = Af_targ[Af_targ.targ_ind == 1]
    Af_targ = Af_targ[['space_name','affluence','volume']]
    Gen_tot = gen_tot.groupby(['space_name','gender']).sum().reset_index()
    Gen_targ = gen_targ.groupby(['space_name','gender','targ_ind']).sum().reset_index()
    Gen_targ = Gen_targ[Gen_targ.targ_ind == 1]
    Gen_targ = Gen_targ[['space_name','gender','volume']]
    Age_tot = age_tot.groupby(['space_name','age']).sum().reset_index()
    Age_targ = age_targ.groupby(['space_name','age','targ_ind']).sum().reset_index()
    Age_targ = Age_targ[Age_targ.targ_ind == 1]
    Age_targ = Age_targ[['space_name','age','volume']]
    Ff_sec_tot = ff_tot.groupby(['space_name','Weekday_label']).sum().reset_index()
    Ff_sec_targ = ff_targ.groupby(['space_name','Weekday_label','targ_ind']).sum().reset_index()
    Ff_sec_targ = Ff_sec_targ[Ff_sec_targ.targ_ind == 1]
    Ff_sec_targ = Ff_sec_targ[['space_name','Weekday_label','volume']]

    Af_sec = pd.merge(Af_targ,
                    Af_tot[['space_name','affluence','volume']],
                           left_on=['space_name','affluence'],
                           right_on=['space_name','affluence'],
                           how='right')
    Af_sec = Af_sec.fillna(0)
    if len(spend) > 0:
        Af_p = Af_sec['affluence'].isin(spend)
    else:
        Af_p = Af_sec['affluence'].isin([0,1,2,3,4])
    Af_sec['targ_group'] = np.where(Af_p==True, Af_sec['volume_y'], 0)
    Af_per1 = Af_sec[['space_name','targ_group','volume_y']]
    Af_per = Af_per1.groupby(['space_name']).sum().reset_index()
    Af_per['targ_perc'] = round(((Af_per['targ_group']/Af_per['volume_y'])),2)
#    Af_sec['volume']=Af_sec['volume']/4


    Gen_sec = pd.merge(Gen_targ,
                    Gen_tot[['space_name','gender','volume']],
                           left_on=['space_name','gender'],
                           right_on=['space_name','gender'],
                           how='right')
    Gen_sec = Gen_sec.fillna(0)
#    Gen_sec['volume']=Gen_sec['volume']/4

    Age_sec = pd.merge(Age_targ,
                    Age_tot[['space_name','age','volume']],
                           left_on=['space_name','age'],
                           right_on=['space_name','age'],
                           how='right')
    Age_sec = Age_sec.fillna(0)
#    Age_sec['volume']=Age_sec['volume']/4
    if len(age) > 0:
        age_p = Age_sec['age'].isin(age)
    else:
        age_p = Age_sec['age'].isin([0,1,2,3,4,5])
    Age_sec['targ_group'] = np.where(age_p==True, Age_sec['volume_y'], 0)
    age_per1 = Age_sec[['space_name','targ_group','volume_y']]
    age_per = age_per1.groupby(['space_name']).sum().reset_index()
    age_per['targ_perc'] = round(((age_per['targ_group']/age_per['volume_y'])),2)


    Ff_sec = pd.merge(Ff_sec_targ,
                    Ff_sec_tot[['space_name','Weekday_label','volume']],
                           left_on=['space_name','Weekday_label'],
                           right_on=['space_name','Weekday_label'],
                           how='right')
    Ff_sec = Ff_sec.fillna(0)

    gen_per = space_info[['space_name','gender','volume']]
    fin_gen_sum = gen_per.groupby(['space_name','gender']).sum().reset_index()
    fin_gen_m = fin_gen_sum[fin_gen_sum['gender']==0].reset_index()
    fin_gen_f = fin_gen_sum[fin_gen_sum['gender']==1].reset_index()
    fin_gen_m = fin_gen_m[['space_name','volume']]
    fin_gen_f = fin_gen_f[['space_name','volume']]

    gen_fin = pd.merge(fin_gen_m,
                     fin_gen_f[['space_name','volume']],
                           left_on=['space_name'],
                           right_on=['space_name'],
                           how='left')

    gen_fin['Male'] = round((gen_fin['volume_x']/(gen_fin['volume_x']+gen_fin['volume_y'])),2)
    gen_fin['Female']= 1-gen_fin['Male']
    gen_fin = gen_fin[['space_name','Male','Female']]

#top age groups
    age_top = Fin_data2[['space_name','age','volume']]
    age_top_sum = age_top.groupby(['space_name','age']).sum().reset_index()
    per_cre = age_top_sum.groupby(['space_name','age']).agg({'volume': 'sum'})
    age_top_per = per_cre.groupby(level=0).apply(lambda x:
                                            100 * x / float(x.sum())).reset_index()
    age_top_per['percent'] = round(age_top_per['volume'])
    if len(age) > 0:
        age_qt = age_top_per.age.isin(age)
    else:
        age_qt = age_top_per.age.isin([0,1,2,3,4,5])    
    age_top_per = age_top_per[age_qt]
    age_top_per = age_top_per.loc[age_top_per.groupby('space_name').volume.idxmax()].reset_index()

    age_top = json.loads(age_top_per.to_json(orient='records')) 
    ff = json.loads(Ff_sec.to_json(orient='records'))
    Age = json.loads(Age_sec.to_json(orient='records'))
    Ageper = json.loads(age_per.to_json(orient='records'))
    Gen = json.loads(Gen_sec.to_json(orient='records'))
    Af = json.loads(Af_sec.to_json(orient='records'))
    Afper = json.loads(Af_per.to_json(orient='records'))
    gen_per = json.loads(gen_fin.to_json(orient='records')) 
    data_return={}
    data_return['Footfall']=ff 
    data_return['Age']=Age
    data_return['Gender']=Gen
    data_return['Affluence']=Af
    data_return['Gen_Per']=gen_per
    data_return['Age_per']=Ageper
    data_return['Age_top']=age_top
    data_return['Spend_Per']=Afper


    return jsonify(data_return)



CORS(app,resources={"/*": {"origins": "*"}},send_wildcard=True)

if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        db.create_all()
    app.run(host='0.0.0.0', ssl_context='adhoc', debug=True)
